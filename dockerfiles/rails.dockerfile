ARG RUBY
FROM ruby:${RUBY}

RUN \
  echo "debconf debconf/frontend select Noninteractive" | \
    debconf-set-selections

RUN \
  echo 'APT::Install-Recommends "false";' > \
    /etc/apt/apt.conf.d/disable-install-recommends

RUN \
  apt update && \
  apt install -y -V \
    libpq-dev \
    npm \
    sudo \
    yarnpkg && \
  apt clean && \
  rm -rf /var/lib/apt/lists/*

RUN \
  gem install bundler

RUN \
  useradd --user-group --create-home rails

RUN \
  echo "rails ALL=(ALL:ALL) NOPASSWD:ALL" | \
    EDITOR=tee visudo -f /etc/sudoers.d/rails

USER rails
WORKDIR /home/rails

COPY Gemfile .
RUN bundle config path vendor/bundle
RUN bundle install
