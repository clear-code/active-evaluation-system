class CreateEvaluationTargets < ActiveRecord::Migration[7.1]
  def change
    create_table :evaluation_targets do |t|
      t.references :period, null: false, foreign_key: true
      t.references :evaluatee, null: false, foreign_key: { to_table: :users }

      t.timestamps

      t.index [:period_id, :evaluatee_id], unique: true
    end
  end
end
