class AddUrlIndexToEvents < ActiveRecord::Migration[7.1]
  def change
    add_index :events, :url
  end
end
