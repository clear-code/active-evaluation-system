class CreateEvaluations < ActiveRecord::Migration[6.0]
  def change
    create_table :evaluations do |t|
      t.references :assignment, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: true
      t.float :value
      t.text :comment

      t.timestamps
    end

    add_index :evaluations, :value
  end
end
