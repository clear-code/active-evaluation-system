class AddForGroupToItemTags < ActiveRecord::Migration[6.0]
  def change
    add_column :item_tags, :for_group, :boolean
  end
end
