class CreateAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :assignments do |t|
      t.references :period, null: false, foreign_key: true
      t.references :evaluatee, null: false, foreign_key: { to_table: :users }
      t.references :evaluator, null: false, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
