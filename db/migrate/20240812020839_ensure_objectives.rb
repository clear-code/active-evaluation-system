class EnsureObjectives < ActiveRecord::Migration[7.1]
  class EvaluationTarget < ActiveRecord::Base
    has_many :objectives
  end

  class ItemTag < ActiveRecord::Base
    has_many :objectives
  end

  class Objective < ActiveRecord::Base
    belongs_to :evaluation_target
    belongs_to :item_tag
  end

  def up
    ActiveRecord::Base.transaction do
      all_item_tags = ItemTag.order(:name).to_a
      EvaluationTarget.all.find_each do |target|
        next if target.objectives.exists?
        all_item_tags.each do |item_tag|
          Objective.create!(evaluation_target: target,
                            item_tag: item_tag)
        end
      end
    end
  end
end
