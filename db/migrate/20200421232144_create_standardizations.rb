class CreateStandardizations < ActiveRecord::Migration[6.0]
  def change
    create_table :standardizations do |t|
      t.references :standard, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: true

      t.timestamps
    end
  end
end
