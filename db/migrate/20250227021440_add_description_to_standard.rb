class AddDescriptionToStandard < ActiveRecord::Migration[7.1]
  def change
    add_column :standards, :description, :text
  end
end
