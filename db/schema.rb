# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2025_02_27_021440) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgroonga"
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.bigint "evaluator_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "evaluation_target_id", null: false
    t.index ["evaluation_target_id", "evaluator_id"], name: "index_assignments_on_evaluation_target_id_and_evaluator_id", unique: true
    t.index ["evaluation_target_id"], name: "index_assignments_on_evaluation_target_id"
    t.index ["evaluator_id"], name: "index_assignments_on_evaluator_id"
  end

  create_table "evaluation_reports", force: :cascade do |t|
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "evaluation_target_id", null: false
    t.index ["evaluation_target_id"], name: "index_evaluation_reports_on_evaluation_target_id", unique: true
  end

  create_table "evaluation_targets", force: :cascade do |t|
    t.bigint "period_id", null: false
    t.bigint "evaluatee_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["evaluatee_id"], name: "index_evaluation_targets_on_evaluatee_id"
    t.index ["period_id", "evaluatee_id"], name: "index_evaluation_targets_on_period_id_and_evaluatee_id", unique: true
    t.index ["period_id"], name: "index_evaluation_targets_on_period_id"
  end

  create_table "evaluations", force: :cascade do |t|
    t.bigint "assignment_id", null: false
    t.bigint "item_id", null: false
    t.float "value"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assignment_id"], name: "index_evaluations_on_assignment_id"
    t.index ["item_id"], name: "index_evaluations_on_item_id"
    t.index ["value"], name: "index_evaluations_on_value"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.string "url"
    t.datetime "occurred_at", precision: nil
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["occurred_at"], name: "index_events_on_occurred_at"
    t.index ["url"], name: "index_events_on_url"
  end

  create_table "final_evaluations", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.float "value"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "evaluation_target_id", null: false
    t.index ["evaluation_target_id", "item_id"], name: "index_final_evaluations_on_evaluation_target_id_and_item_id", unique: true
    t.index ["evaluation_target_id"], name: "index_final_evaluations_on_evaluation_target_id"
    t.index ["item_id"], name: "index_final_evaluations_on_item_id"
    t.index ["value"], name: "index_final_evaluations_on_value"
  end

  create_table "item_groups", force: :cascade do |t|
    t.text "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_taggings", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.bigint "item_tag_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_item_taggings_on_item_id"
    t.index ["item_tag_id"], name: "index_item_taggings_on_item_tag_id"
  end

  create_table "item_tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string "name"
    t.text "reason"
    t.text "example"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "note"
    t.bigint "item_group_id", null: false
    t.index ["item_group_id"], name: "index_items_on_item_group_id"
  end

  create_table "objectives", force: :cascade do |t|
    t.bigint "evaluation_target_id", null: false
    t.bigint "item_tag_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["evaluation_target_id", "item_tag_id"], name: "index_objectives_on_evaluation_target_id_and_item_tag_id", unique: true
    t.index ["evaluation_target_id"], name: "index_objectives_on_evaluation_target_id"
    t.index ["item_tag_id"], name: "index_objectives_on_item_tag_id"
  end

  create_table "participants", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id", "user_id"], name: "index_participants_on_event_id_and_user_id", unique: true
    t.index ["event_id"], name: "index_participants_on_event_id"
    t.index ["user_id"], name: "index_participants_on_user_id"
  end

  create_table "periods", force: :cascade do |t|
    t.string "name"
    t.date "start_on"
    t.date "end_on"
    t.bigint "standard_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["end_on"], name: "index_periods_on_end_on"
    t.index ["standard_id"], name: "index_periods_on_standard_id"
    t.index ["start_on"], name: "index_periods_on_start_on"
  end

  create_table "standardizations", force: :cascade do |t|
    t.bigint "standard_id", null: false
    t.bigint "item_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_standardizations_on_item_id"
    t.index ["standard_id"], name: "index_standardizations_on_standard_id"
  end

  create_table "standards", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.boolean "admin"
    t.index ["admin"], name: "index_users_on_admin"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "assignments", "evaluation_targets"
  add_foreign_key "assignments", "users", column: "evaluator_id"
  add_foreign_key "evaluation_reports", "evaluation_targets"
  add_foreign_key "evaluation_targets", "periods"
  add_foreign_key "evaluation_targets", "users", column: "evaluatee_id"
  add_foreign_key "evaluations", "assignments"
  add_foreign_key "evaluations", "items"
  add_foreign_key "final_evaluations", "evaluation_targets"
  add_foreign_key "final_evaluations", "items"
  add_foreign_key "item_taggings", "item_tags"
  add_foreign_key "item_taggings", "items"
  add_foreign_key "items", "item_groups"
  add_foreign_key "objectives", "evaluation_targets"
  add_foreign_key "objectives", "item_tags"
  add_foreign_key "participants", "events"
  add_foreign_key "participants", "users"
  add_foreign_key "periods", "standards"
  add_foreign_key "standardizations", "items"
  add_foreign_key "standardizations", "standards"
end
