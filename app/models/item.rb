class Item < ApplicationRecord
  has_many :item_taggings, dependent: :destroy
  has_many :item_tags, -> {order(:name)}, through: :item_taggings
  belongs_to :item_group
  has_many :standardizations, dependent: :destroy
  has_many :standards, through: :standardizations
  has_many :evaluations, dependent: :destroy
  has_many :final_evaluations, dependent: :destroy
  accepts_nested_attributes_for :item_taggings, allow_destroy: true

  validates :item_group, presence: true

  def available_item_taggings
    ItemTag.all.order(:name).collect do |item_tag|
      ItemTagging.find_or_initialize_by(item_id: id,
                                        item_tag_id: item_tag.id)
    end
  end
end
