class ItemTag < ApplicationRecord
  has_many :item_taggings, dependent: :destroy
  has_many :items, -> {order(:id)}, through: :item_taggings
  has_many :objectives, dependent: :destroy
end
