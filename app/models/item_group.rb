class ItemGroup < ApplicationRecord
  has_many :items, -> {order(:id)}, dependent: :destroy

  validates :name, presence: true
end
