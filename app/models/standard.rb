class Standard < ApplicationRecord
  has_many :periods
  has_many :standardizations, dependent: :destroy
  has_many :items,
           -> {includes(:item_tags, :standards).order(:id)},
           through: :standardizations
  accepts_nested_attributes_for :standardizations, allow_destroy: true

  def available_standardizations
    Item.all.order(:id).collect do |item|
      Standardization.find_or_initialize_by(standard_id: id,
                                            item_id: item.id)
    end
  end

  def tags
    @tags ||= find_tags
  end

  def summary_item_tags
    ItemTag.
      where(id: ItemTagging.where(item: items).select(:item_tag_id))
  end

  private
  def find_tags
    tags = {}
    items.
      eager_load(:item_tags).
      each do |item|
      item.item_tags.each do |tag|
        (tags[tag] ||= []) << item
      end
    end
    sorted_tags = {}
    tags.keys.sort_by(&:name).each do |tag|
      sorted_tags[tag] = tags[tag].sort_by do |item|
        item.id
      end
    end
    sorted_tags
  end
end
