class EvaluationTarget < ApplicationRecord
  belongs_to :period
  belongs_to :evaluatee,
             class_name: "User",
             foreign_key: "evaluatee_id"
  has_many :assignments, dependent: :destroy
  has_one :evaluation_report, dependent: :destroy
  has_many :final_evaluations, dependent: :destroy
  has_many :objectives, dependent: :destroy
  has_many :objective_item_tags, through: :objectives, source: :item_tag

  def items
    target_items = period.
                     standard.
                     items.
                     joins(:item_tags).
                     where(item_tags: objective_item_tags)
    period.standard.items.where(id: target_items)
  end

  private def item_sort_key(item)
    [item.item_group_id || -1, item.id]
  end

  def sorted_items
    items.sort_by do |item|
      item_sort_key(item)
    end
  end

  def sorted_final_evaluations
    final_evaluations.
      eager_load(item: :item_tags).
      sort_by do |final_evaluation|
      item_sort_key(final_evaluation.item)
    end
  end

  def grouped_final_evaluations
    grouped = {}
    sorted_final_evaluations.each do |final_evaluation|
      final_evaluation.item.item_tags.each do |item_tag|
        (grouped[item_tag.id] ||= []) << final_evaluation
      end
    end
    grouped
  end
end
