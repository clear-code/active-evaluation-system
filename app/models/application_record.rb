module ActiveModel
  class Name
    def i18n_key_plural
      @i18n_key_plural ||= @plural.underscore.to_sym
    end

    def human_plural(options = {})
      @human_plural ||= ActiveSupport::Inflector.humanize(@plural)

      defaults = @klass.lookup_ancestors.map do |klass|
        klass.model_name.i18n_key_plural
      end

      defaults << options[:default] if options[:default]
      defaults << @human_plural

      options = { scope: [@klass.i18n_scope, :plurals], count: 1, default: defaults }.merge!(options.except(:default))
      I18n.translate(defaults.shift, **options)
    end
  end
end

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
