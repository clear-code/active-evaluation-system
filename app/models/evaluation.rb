class Evaluation < ApplicationRecord
  belongs_to :assignment
  has_one :evaluation_target, through: :assignment
  has_one :evaluatee, through: :evaluation_target
  has_one :period, through: :evaluation_target
  belongs_to :item

  def events
    period.events.joins(:participants).where(participants: {user: evaluatee})
  end
end
