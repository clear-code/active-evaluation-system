class Event < ApplicationRecord
  has_many :participants, dependent: :destroy
  has_many :users, -> {active.order(:id)}, through: :participants
  accepts_nested_attributes_for :participants, allow_destroy: true

  def available_participants
    User.active.order(:id).collect do |user|
      Participant.find_or_initialize_by(event_id: id,
                                        user_id: user.id)
    end
  end
end
