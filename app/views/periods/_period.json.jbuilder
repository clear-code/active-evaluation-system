json.extract! period, :id, :name, :start_on, :end_on, :standard_id, :created_at, :updated_at
json.url period_url(period, format: :json)
