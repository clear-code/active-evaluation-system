json.extract! item, :id, :name, :reason, :example, :note, :created_at, :updated_at
json.url item_url(item, format: :json)
