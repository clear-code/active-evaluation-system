json.extract! item_tagging, :id, :item_id, :item_tag_id, :created_at, :updated_at
json.url item_tagging_url(item_tagging, format: :json)
