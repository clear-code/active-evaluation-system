json.extract! standardization, :id, :standard_id, :item_id, :created_at, :updated_at
json.url standardization_url(standardization, format: :json)
