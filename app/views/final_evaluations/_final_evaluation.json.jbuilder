json.extract! final_evaluation, :id, :period_id, :evaluatee_id, :item_id, :value, :comment, :created_at, :updated_at
json.url final_evaluation_url(final_evaluation, format: :json)
