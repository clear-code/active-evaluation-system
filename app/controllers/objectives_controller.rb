class ObjectivesController < ApplicationController
  before_action :require_admin
  before_action :set_evaluation_target, only: [:create]
  before_action :set_objective, only: [:destroy]

  # POST /evaluation_target/1/objectives
  # POST /evaluation_target/1/objectives.json
  def create
    @objective = Objective.new(objective_params)
    @objective.evaluation_target = @evaluation_target

    respond_to do |format|
      if @objective.save
        format.html do
          redirect_to @evaluation_target,
                      notice: 'Objective was successfully created.'
        end
        format.json { render :show, status: :created, location: @objective }
      else
        format.html do
          redirect_to @evaluation_target,
                      notice: 'Objective was not created.'
        end
        format.json { render json: @objective.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /objectives/1
  # DELETE /objectives/1.json
  def destroy
    @objective.destroy
    respond_to do |format|
      format.html { redirect_to @evaluation_target, notice: 'Objective was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_evaluation_target
    @evaluation_target = EvaluationTarget.find(params[:evaluation_target_id])
  end

  def set_objective
    @objective = Objective.find(params[:id])
    @evaluation_target = @objective.evaluation_target
  end

  # Only allow a list of trusted parameters through.
  def objective_params
    params.require(:objective).permit(:item_tag_id)
  end
end
