class PeriodsController < ApplicationController
  before_action :require_admin, except: [:index, :show]
  before_action :set_period, only: [:show, :edit, :update, :destroy]

  # GET /periods
  # GET /periods.json
  def index
    @periods = Period.order(start_on: :desc)
  end

  # GET /periods/1
  # GET /periods/1.json
  def show
    @evaluation_targets =
      @period.
        evaluation_targets.
        eager_load(:evaluatee, :evaluation_report).
        order(:evaluatee_id)
    @summary_item_tags = @period.standard.summary_item_tags
    @summary_values = {}
    FinalEvaluation.
      joins(evaluation_target: [], item: :item_tags).
      where(evaluation_target: {period: @period},
            item: {item_tags: @summary_item_tags}).
      group("evaluation_target_id",
            "item_tag_id").
      select("evaluation_target_id",
             "item_tag_id",
             "sum(value) as total_value").
      each do |final_evaluation|
      target = final_evaluation.evaluation_target
      @summary_values[[target.evaluatee_id, final_evaluation.item_tag_id]] =
        final_evaluation.total_value
    end
  end

  # GET /periods/new
  def new
    @period = Period.new
  end

  # GET /periods/1/edit
  def edit
  end

  # POST /periods
  # POST /periods.json
  def create
    @period = Period.new(period_params)

    respond_to do |format|
      if @period.save
        format.html { redirect_to @period, notice: 'Period was successfully created.' }
        format.json { render :show, status: :created, location: @period }
      else
        format.html { render :new }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /periods/1
  # PATCH/PUT /periods/1.json
  def update
    respond_to do |format|
      if @period.update(period_params)
        format.html { redirect_to @period, notice: 'Period was successfully updated.' }
        format.json { render :show, status: :ok, location: @period }
      else
        format.html { render :edit }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /periods/1
  # DELETE /periods/1.json
  def destroy
    @period.destroy
    respond_to do |format|
      format.html { redirect_to periods_url, notice: 'Period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_period
      @period = Period.find(params[:id])
      @assignments = @period.assignments.to_a
    end

    # Only allow a list of trusted parameters through.
    def period_params
      params.require(:period).permit(:name,
                                     :start_on,
                                     :end_on,
                                     :standard_id,
                                     assignments_attributes: [
                                       :id,
                                       :evaluatee_id,
                                       :evaluator_id,
                                       :_destroy,
                                     ])
    end
end
