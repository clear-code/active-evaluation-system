class EvaluationTargetsController < ApplicationController
  before_action :require_admin, except: [:index]
  before_action :set_period, only: [:index, :create]
  before_action :set_users, only: [:index, :show, :create]
  before_action :set_evaluation_targets, only: [:index, :create]
  before_action :set_evaluation_target, only: [:show, :destroy]
  before_action :set_item_tags, only: [:show]
  before_action :set_assignments, only: [:show]

  # GET /periods/1/evaluation_targets
  # GET /periods/1/evaluation_targets.json
  def index
    @evaluation_target = nil
  end

  # GET /evaluation_targets/1
  # GET /evaluation_targets/1.json
  def show
  end

  # POST /periods/1/evaluation_targets
  # POST /periods/1/evaluation_targets.json
  def create
    @evaluation_target = EvaluationTarget.new(evaluation_target_params)
    @evaluation_target.period = @period

    respond_to do |format|
      if @evaluation_target.save
        format.html do
          redirect_to period_evaluation_targets_url(@period),
                      notice: 'Evaluation target was successfully created.'
        end
        format.json { render :show, status: :created, location: @evaluation_target }
      else
        format.html { render :index }
        format.json { render json: @evaluation_target.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluation_targets/1
  # DELETE /evaluation_targets/1.json
  def destroy
    @evaluation_target.destroy
    respond_to do |format|
      format.html do
        redirect_to period_evaluation_targets_url(@evaluation_target),
                    notice: 'Objective was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private
  def set_period
    @period = Period.find(params[:period_id])
  end

  def set_users
    @users = User.active.order(:id)
  end

  def set_evaluation_targets
    @evaluation_targets = {}
    @period.
      evaluation_targets.
      eager_load(objectives: [:item_tag],
                 assignments: [:period, :evaluatee, :evaluator]).
      each do |target|
      @evaluation_targets[target.evaluatee_id] = target
    end
  end

  def set_evaluation_target
    @evaluation_target = EvaluationTarget.find(params[:id])
    @period = @evaluation_target.period
  end

  def set_item_tags
    @item_tags = ItemTag.order(:name)
    @selected_objectives = {}
    @evaluation_target.objectives.each do |objective|
      @selected_objectives[objective.item_tag_id] = objective
    end
  end

  def set_assignments
    @assignments = {}
    @period.
      assignments.
      where(evaluation_target: @evaluation_target).
      each do |assignment|
      @assignments[assignment.evaluator_id] = assignment
    end
  end

  # Only allow a list of trusted parameters through.
  def evaluation_target_params
    params.require(:evaluation_target).permit(:evaluatee_id)
  end
end
