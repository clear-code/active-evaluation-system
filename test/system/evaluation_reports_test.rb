require "application_system_test_case"

class EvaluationReportsTest < ApplicationSystemTestCase
  setup do
    @evaluation_report = evaluation_reports(:one)
  end

  test "visiting the index" do
    visit evaluation_reports_url
    assert_selector "h1", text: "Evaluation Reports"
  end

  test "creating a Evaluation report" do
    visit evaluation_reports_url
    click_on "New Evaluation Report"

    fill_in "Content", with: @evaluation_report.content
    fill_in "Evaluatee", with: @evaluation_report.evaluatee_id
    fill_in "Period", with: @evaluation_report.period_id
    click_on "Create Evaluation report"

    assert_text "Evaluation report was successfully created"
    click_on "Back"
  end

  test "updating a Evaluation report" do
    visit evaluation_reports_url
    click_on "Edit", match: :first

    fill_in "Content", with: @evaluation_report.content
    fill_in "Evaluatee", with: @evaluation_report.evaluatee_id
    fill_in "Period", with: @evaluation_report.period_id
    click_on "Update Evaluation report"

    assert_text "Evaluation report was successfully updated"
    click_on "Back"
  end

  test "destroying a Evaluation report" do
    visit evaluation_reports_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Evaluation report was successfully destroyed"
  end
end
