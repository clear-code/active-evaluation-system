require "application_system_test_case"

class ItemGroupsTest < ApplicationSystemTestCase
  setup do
    @item_group = item_groups(:one)
  end

  test "visiting the index" do
    visit item_groups_url
    assert_selector "h1", text: "Item groups"
  end

  test "should create item group" do
    visit item_groups_url
    click_on "New item group"

    fill_in "Name", with: @item_group.name
    click_on "Create Item group"

    assert_text "Item group was successfully created"
    click_on "Back"
  end

  test "should update Item group" do
    visit item_group_url(@item_group)
    click_on "Edit this item group", match: :first

    fill_in "Name", with: @item_group.name
    click_on "Update Item group"

    assert_text "Item group was successfully updated"
    click_on "Back"
  end

  test "should destroy Item group" do
    visit item_group_url(@item_group)
    click_on "Destroy this item group", match: :first

    assert_text "Item group was successfully destroyed"
  end
end
