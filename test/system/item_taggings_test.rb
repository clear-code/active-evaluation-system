require "application_system_test_case"

class ItemTaggingsTest < ApplicationSystemTestCase
  setup do
    @item_tagging = item_taggings(:one)
  end

  test "visiting the index" do
    visit item_taggings_url
    assert_selector "h1", text: "Item Taggings"
  end

  test "creating a Item tagging" do
    visit item_taggings_url
    click_on "New Item Tagging"

    fill_in "Item", with: @item_tagging.item_id
    fill_in "Item tag", with: @item_tagging.item_tag_id
    click_on "Create Item tagging"

    assert_text "Item tagging was successfully created"
    click_on "Back"
  end

  test "updating a Item tagging" do
    visit item_taggings_url
    click_on "Edit", match: :first

    fill_in "Item", with: @item_tagging.item_id
    fill_in "Item tag", with: @item_tagging.item_tag_id
    click_on "Update Item tagging"

    assert_text "Item tagging was successfully updated"
    click_on "Back"
  end

  test "destroying a Item tagging" do
    visit item_taggings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Item tagging was successfully destroyed"
  end
end
