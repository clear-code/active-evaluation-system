require 'test_helper'

class EvaluationReportsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @evaluation_report = evaluation_reports("2020Q1-alice")
    sign_in users(:bob)
  end

  test "should show evaluation_report" do
    get evaluation_report_url(@evaluation_report)
    assert_response :success
  end

  test "should get edit" do
    get edit_evaluation_report_url(@evaluation_report)
    assert_response :success
  end

  test "should update evaluation_report" do
    patch evaluation_report_url(@evaluation_report),
          params: {
            evaluation_report: {
              content: @evaluation_report.content,
              evaluation_target_id: @evaluation_report.evaluation_target_id,
            }
          }
    assert_redirected_to evaluation_report_url(@evaluation_report)
  end

  test "should destroy evaluation_report" do
    period = @evaluation_report.period

    assert_difference('EvaluationReport.count', -1) do
      delete evaluation_report_url(@evaluation_report)
    end

    assert_redirected_to period
  end
end
