require 'test_helper'

class FinalEvaluationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @final_evaluation = final_evaluations("2020Q1-alice-fix-in-upstream")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get evaluation_target_final_evaluations_url(@final_evaluation.evaluation_target)
    assert_response :success
  end

  test "should show final_evaluation" do
    sign_in users(:alice)
    get final_evaluation_url(@final_evaluation)
    assert_response :success
  end

  test "should get edit" do
    get edit_final_evaluation_url(@final_evaluation)
    assert_response :success
  end

  test "should update final_evaluation" do
    patch final_evaluation_url(@final_evaluation),
          params: {
            final_evaluation: {
              comment: @final_evaluation.comment,
              evaluation_target_id: @final_evaluation.evaluation_target_id,
              item_id: @final_evaluation.item_id,
              value: @final_evaluation.value
            }
          }
    assert_redirected_to period_url(@final_evaluation.period)
  end
end
