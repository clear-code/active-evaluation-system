require 'test_helper'

class StandardizationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @standardization = standardizations("2020-01-fix-in-upstream")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get standardizations_url
    assert_response :success
  end

  test "should get new" do
    get new_standardization_url
    assert_response :success
  end

  test "should create standardization" do
    assert_difference('Standardization.count') do
      post standardizations_url, params: { standardization: { item_id: @standardization.item_id, standard_id: @standardization.standard_id } }
    end

    assert_redirected_to standardization_url(Standardization.last)
  end

  test "should show standardization" do
    sign_in users(:alice)
    get standardization_url(@standardization)
    assert_response :success
  end

  test "should get edit" do
    get edit_standardization_url(@standardization)
    assert_response :success
  end

  test "should update standardization" do
    patch standardization_url(@standardization), params: { standardization: { item_id: @standardization.item_id, standard_id: @standardization.standard_id } }
    assert_redirected_to standardization_url(@standardization)
  end

  test "should destroy standardization" do
    assert_difference('Standardization.count', -1) do
      delete standardization_url(@standardization)
    end

    assert_redirected_to standardizations_url
  end
end
