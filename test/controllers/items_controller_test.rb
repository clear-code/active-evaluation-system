require 'test_helper'

class ItemsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @item = items("fix-in-upstream")
    sign_in users(:bob)
  end

  test "should get index" do
    sign_in users(:alice)
    get items_url
    assert_response :success
  end

  test "should get new" do
    get new_item_url
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post items_url,
           params: {
             item: {
               example: @item.example,
               name: @item.name,
               reason: @item.reason,
               item_group_id: item_groups("development-style").id,
             },
           }
    end

    assert_redirected_to item_url(Item.last)
  end

  test "should show item" do
    sign_in users(:alice)
    get item_url(@item)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_url(@item)
    assert_response :success
  end

  test "should update item" do
    patch item_url(@item), params: { item: { example: @item.example, name: @item.name, reason: @item.reason } }
    assert_redirected_to item_url(@item)
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete item_url(@item)
    end

    assert_redirected_to items_url
  end
end
