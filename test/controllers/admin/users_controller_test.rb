require 'test_helper'

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users("chris")
    sign_in users(:bob)
  end

  test "should not get index with normal user" do
    sign_in users(:alice)
    get admin_users_url
    assert_redirected_to root_url
  end

  test "should get index" do
    get admin_users_url
    assert_response :success
  end

  test "should show user" do
    get admin_user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch admin_user_url(@user), params: { user: { name: @user.name } }
    assert_redirected_to admin_user_url(@user)
  end
end
