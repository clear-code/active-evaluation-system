Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :events
  resources :item_groups
  resources :item_taggings
  resources :item_tags
  devise_for :users
  namespace :admin do
    resources :users
  end
  resources :standardizations
  resources :items
  resources :periods do
    resources :evaluation_targets,
              shallow: true,
              only: [:index, :show, :create, :destroy] do
      resources :assignments,
                shallow: true,
                only: [:create, :destroy] do
        resources :evaluations, only: [:index, :show, :edit, :update] do
          collection do
            post "synchronize"
          end
        end
      end
      resources :objectives, only: [:create, :destroy]
      resources :final_evaluations, only: [:index, :show, :edit, :update] do
        collection do
          post "synchronize"
        end
      end
      resources :evaluation_reports,
                shallow: true,
                except: [:index]
    end
  end
  resources :standards
  root "events#index"
end
